{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Tezos.Common.Json where

import Control.DeepSeq (NFData)
import Data.Aeson (FromJSON, ToJSON, Value, camelTo2, encode, parseJSON, toEncoding, toJSON)
import qualified Data.Aeson.Encoding as AE
import Data.Bits (Bits)
import Data.Coerce (coerce)
import Data.Hashable (Hashable)
import Data.Int (Int64)
import Data.List (uncons)
import Data.Proxy (Proxy (..))
#if !(MIN_VERSION_base(4,11,0))
import Data.Semigroup
#endif
import qualified Data.Aeson.TH as Aeson
import qualified Data.Aeson.Types as Aeson
import qualified Data.Text as T
import Data.Typeable (Typeable, typeRep)
import GHC.Generics
import Language.Haskell.TH
import Numeric.Natural
import Text.Read (Read (readsPrec, readList, readPrec, readListPrec), ReadS, ReadPrec, readMaybe)
import Text.Show (Show(showsPrec, show, showList), ShowS)

parseAsString :: forall a. (Read a, Typeable a) => Aeson.Value -> Aeson.Parser a
parseAsString = Aeson.withText (show $ typeRep (Proxy :: Proxy a)) $ \txt ->
  maybe (fail "Failed to parse string") pure $ readMaybe (T.unpack txt)

parseStringEncodedIntegral :: (Read a, Integral a, Typeable a) => Aeson.Value -> Aeson.Parser (StringEncode a)
parseStringEncodedIntegral x = StringEncode <$> parseAsString x

deriveTezosJson :: Name -> Q [Dec]
deriveTezosJson = deriveTezosJsonKind "kind"

deriveTezosToJson :: Name -> Q [Dec]
deriveTezosToJson = Aeson.deriveToJSON (tezosJsonOptionsKind "kind")

deriveTezosJsonKind :: String -> Name -> Q [Dec]
deriveTezosJsonKind = Aeson.deriveJSON . tezosJsonOptionsKind

tezosJsonOptions :: Aeson.Options
tezosJsonOptions = tezosJsonOptionsKind "kind"

tezosJsonOptionsKind :: String -> Aeson.Options
tezosJsonOptionsKind tagFieldName = Aeson.defaultOptions
  { Aeson.fieldLabelModifier = tail . camelTo2 '_' . dropWhile ('_' /=) . tail
  , Aeson.constructorTagModifier =
      \ctor -> camelTo2 '_' $ maybe ctor snd $ uncons $ dropWhile ('_' /=) ctor
  , Aeson.sumEncoding = Aeson.defaultTaggedObject
    { Aeson.tagFieldName = tagFieldName
    }
  }

-- | JavaScript compatibility forces us and the Tezos JSON/RPC to encode any
-- integer with more than 32 bits into a decimal string format. StringEncode
-- helps us safely represent a range of different types for which we must always
-- respect this property.
newtype StringEncode a = StringEncode { unStringEncode :: a }
  deriving (Generic, Eq, Ord, Bounded, Enum, Typeable, Num, Integral, Bits, Real, NFData, Hashable)

-- TODO just use deriving strategies when we're going to only support GHC 8.2+
instance Read a => Read (StringEncode a) where
  readsPrec = (coerce :: (Int -> ReadS a) -> Int -> ReadS (StringEncode a)) readsPrec
  readList = (coerce :: ReadS [a] -> ReadS [StringEncode a]) readList
  readPrec = (coerce :: ReadPrec a -> ReadPrec (StringEncode a)) readPrec
  readListPrec = (coerce :: ReadPrec [a] -> ReadPrec [StringEncode a]) readListPrec

instance Show a => Show (StringEncode a) where
  showsPrec = (coerce :: (Int -> a -> ShowS) -> Int -> StringEncode a -> ShowS) showsPrec
  show = (coerce :: (a -> String) -> (StringEncode a) -> String) show
  showList = (coerce :: ([a] -> ShowS) -> [StringEncode a] -> ShowS) showList

type TezosInt64 = StringEncode Int64

type TezosBigNum = StringEncode Integer

-- | WARNING: Due to a major oversight on the part of Tezos Core, this type is
-- incorrectly named. It represents _non-negative_ integers (including zero),
-- not _strictly positive_ integers. Therefore the Haskell Natural type is
-- indeed the correct counterpart.
type TezosPositiveBigNum = StringEncode Natural

instance FromJSON TezosInt64 where
  parseJSON = parseStringEncodedIntegral

instance ToJSON TezosInt64 where
  toJSON (StringEncode x) = Aeson.String $ T.pack $ show x
  toEncoding (StringEncode x) = AE.int64Text x

instance FromJSON TezosBigNum where
  parseJSON = parseStringEncodedIntegral

instance ToJSON TezosBigNum where
  toJSON (StringEncode x) = Aeson.String $ T.pack $ show x
  toEncoding (StringEncode x) = AE.integerText x

instance FromJSON TezosPositiveBigNum where
  parseJSON = parseStringEncodedIntegral

instance ToJSON TezosPositiveBigNum where
  toJSON (StringEncode x) = Aeson.String $ T.pack $ show x
  toEncoding (StringEncode x) = AE.integerText (fromIntegral x)

-- | "$ref": "#/definitions/error",
newtype JsonRpcError = JsonRpcError Value
  deriving (Eq, Show, Typeable, ToJSON, FromJSON)

instance Ord JsonRpcError where
  compare (JsonRpcError a) (JsonRpcError b) = encode a `compare` encode b
