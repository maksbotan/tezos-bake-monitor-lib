{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Tezos.V005.Entrypoint.Internal where

import Control.Monad.Fail (MonadFail(fail))
import qualified Data.ByteString.Char8 as C8
import Data.Typeable (Typeable)
import Prelude hiding (fail)

newtype Fail a = Fail { getFail :: Either String a }
  deriving (Show,Read,Eq,Ord,Functor,Applicative,Monad,Typeable)

instance MonadFail Fail where
  fail = Fail . Left

class MightBeString a where
  fishString :: String -> Either String a
  toString :: a -> C8.ByteString

-- TODO default ToJSON, FromJSON, IsString via MightBeString when our support target >= 8.6 for DerivingVia