{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
module Tezos.V005.Entrypoint where

import Control.Arrow (left)
import Control.Lens.TH (makeLenses, makePrisms)
import Control.Monad (replicateM, when)
import Control.Monad.Fail (MonadFail, fail)
import Data.Aeson (FromJSON(parseJSON), ToJSON(toJSON))
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as C8
import Data.Char (isLatin1)
import Data.Foldable (traverse_)
import Data.String (IsString(fromString))
import Data.Word (Word8)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Data.Typeable (Typeable)
import Prelude hiding (fail)

import qualified Tezos.Common.Binary as B
import Tezos.V005.Contract (ContractId, toContractIdText, tryReadContractIdText)
import Tezos.V005.Entrypoint.Internal

-- This should only be ascii, so ByteString.C8 actually makes sense
newtype EntrypointName = EntrypointName { unEntrypointName :: C8.ByteString }
  deriving (Eq, Ord, Show, Typeable)

entrypointName :: MonadFail m => C8.ByteString -> m EntrypointName
entrypointName bs =
  if len < 32
    then pure (EntrypointName bs)
    else fail "Entrypoint must not exceed 31 characters"
  where
    len = C8.length bs

data Entrypoint
  = EntrypointDefault -- "default"
  | EntrypointRoot -- "root"
  | EntrypointDo -- "do"
  | EntrypointSetDelegate -- "set_delegate"
  | EntrypointRemoveDelegate -- "remove_delegate"
  | EntrypointOther !EntrypointName
  deriving (Eq, Ord, Show, Typeable)

instance ToJSON EntrypointName where
  toJSON = toJSON . C8.unpack . toString

instance FromJSON EntrypointName where
  parseJSON jv = do
    str <- parseJSON @String jv
    either fail pure $ fishString str

instance MightBeString EntrypointName where
  fishString str = do
    when (any (not . isLatin1) str) $ Left "Non latin1 characters found in entrypoint name"
    getFail $ entrypointName (C8.pack str)
  toString = unEntrypointName

instance IsString EntrypointName where
  fromString = either error id . fishString

instance B.TezosBinary EntrypointName where
  put (EntrypointName bs) = B.put @Word8 (fromIntegral $ C8.length bs) *> traverse_ B.put (BS.unpack bs)
  get = do
    lenWord <- B.get @Word8
    let lenInt = fromIntegral lenWord
    when (lenInt > 31) $ fail ("Entry point name length exceeds 31: " <> show lenInt)
    wordsList <- replicateM lenInt (B.get @Word8)
    entrypointName (BS.pack wordsList)

instance ToJSON Entrypoint where
  toJSON = toJSON . C8.unpack . toString

instance FromJSON Entrypoint where
  parseJSON jv = do
    str <- parseJSON @String jv
    either fail pure $ fishString str

instance MightBeString Entrypoint where
  fishString = \case
    "default" -> pure EntrypointDefault
    "root" -> pure EntrypointRoot
    "do" -> pure EntrypointDo
    "set_delegate" -> pure EntrypointSetDelegate
    "remove_delegate" -> pure EntrypointRemoveDelegate
    x -> EntrypointOther <$> fishString x
  toString = \case
    EntrypointDefault -> "default"
    EntrypointRoot -> "root"
    EntrypointDo -> "do"
    EntrypointSetDelegate -> "set_delegate"
    EntrypointRemoveDelegate -> "remove_delegate"
    EntrypointOther t -> toString t

instance IsString Entrypoint where
  fromString = either error id . fishString

instance B.TezosBinary Entrypoint where
  put = \case
    EntrypointDefault -> B.put @Word8 0
    EntrypointRoot -> B.put @Word8 1
    EntrypointDo -> B.put @Word8 2
    EntrypointSetDelegate -> B.put @Word8 3
    EntrypointRemoveDelegate -> B.put @Word8 4
    EntrypointOther epn -> B.put @Word8 255 *> B.put epn

  get = do
    epWord <- B.get @Word8
    case epWord of
      0 -> pure EntrypointDefault
      1 -> pure EntrypointRoot
      2 -> pure EntrypointDo
      3 -> pure EntrypointSetDelegate
      4 -> pure EntrypointRemoveDelegate
      255 -> EntrypointOther <$> B.get
      _ -> fail $ "Unreserved entrypoint code: " <> show epWord

data EntryAddress = EntryAddress
  { _entryAddress_contract :: ContractId
  , _entryAddress_entrypoint :: Entrypoint
  } deriving (Eq, Ord, Show, Typeable)

instance ToJSON EntryAddress where
  toJSON = toJSON . C8.unpack . toString

instance FromJSON EntryAddress where
  parseJSON jv = do
    str <- parseJSON @String jv
    either fail pure $ fishString str

instance MightBeString EntryAddress where
  fishString str = do
    let (addrpart, eppart) = span (/= '%') str
    addr <- left show $ tryReadContractIdText $ T.pack addrpart -- TODO show error better
    ep <- case eppart of
      [] -> pure EntrypointDefault
      _:epname -> fishString epname
    pure $ EntryAddress addr ep
  toString (EntryAddress c EntrypointDefault) = T.encodeUtf8 (toContractIdText c)
  toString (EntryAddress c ep) = T.encodeUtf8 (toContractIdText c) <> "%" <> toString ep

instance IsString EntryAddress where
  fromString = either error id . fishString

instance B.TezosBinary EntryAddress where
  put (EntryAddress addr EntrypointDefault) = B.put addr
  put (EntryAddress addr ep) = B.put addr *> B.put (toString ep)
  get = do
    addr <- B.get
    epstr <- B.get
    ep <- if C8.null epstr
      then pure EntrypointDefault
      else either fail pure $ fishString $ C8.unpack $ epstr
    pure $ EntryAddress addr ep

toEntrypointText :: Entrypoint -> T.Text
toEntrypointText = T.decodeUtf8 . toString

toEntryAddressText :: EntryAddress -> T.Text
toEntryAddressText = T.decodeUtf8 . toString

makePrisms ''Entrypoint
makeLenses ''EntryAddress