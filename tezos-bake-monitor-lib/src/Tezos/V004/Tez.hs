{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Tezos.V004.Tez
  ( Tez(..)
  , getMicroTez
  , microTez
  , TezDelta(..)
  , getMicroTezDelta
  , microTezDelta
  )
  where

import Control.DeepSeq (NFData)
import Data.Aeson (FromJSON, ToJSON, parseJSON, toEncoding, toJSON)
import Data.Fixed (E6, Fixed, Micro, resolution)
import Data.Hashable (Hashable)
import Data.Int (Int64)
import Data.Proxy (Proxy (..))
import Data.Typeable (Typeable)
import Data.Vector.Instances ()
import GHC.Generics (Generic)
import Numeric.Natural (Natural)
import Tezos.V004.Micheline (Expression(Expression_Int))
import Tezos.V004.Michelson (FromMicheline(fromMicheline), ToMicheline(toMicheline))

import qualified Tezos.Common.Binary as B
import Tezos.Common.Json

-- In the json schema this is called mutez and it must be positive (this is the value used as
-- an account balance).
newtype Tez = Tez { getTez :: Micro }
  deriving (Eq, Ord, Show, Typeable, Enum, Fractional, Num, Real, RealFrac, NFData, Generic, Hashable, Read)

getMicroTez :: Tez -> Natural
getMicroTez = fromMicro . getTez

microTez :: Natural -> Tez
microTez = Tez . toMicro

-- | the instance for Data.Fixed.Micro defined in Data.Aeson is perfectly
-- cromulent, its just not what we need.  tezos encodes these values as
-- strings.
instance ToJSON Tez where
  toJSON = toJSON . StringEncode . getMicroTez
  toEncoding = toEncoding . StringEncode . getMicroTez

instance FromJSON Tez where
  parseJSON x = microTez . fromIntegral <$> parseJSON @TezosPositiveBigNum x

instance B.TezosBinary Tez where
  put t = B.put @Natural $ fromIntegral $ getMicroTez t
  get = microTez <$> B.get @Natural

-- Used in BalanceUpdate.
newtype TezDelta = TezDelta { getTezDelta :: Micro }
  deriving (Eq, Ord, Show, Typeable, Enum, Fractional, Num, Real, RealFrac, NFData, Hashable)

-- TODO FIXME: This will need to be changed from Int64 to a type equivalent to
-- Integer when it is fixed upstream in tezos core.
getMicroTezDelta :: TezDelta -> Int64
getMicroTezDelta = fromMicro . getTezDelta

microTezDelta :: Int64 -> TezDelta
microTezDelta = TezDelta . toMicro

instance ToJSON TezDelta where
  toJSON = toJSON . StringEncode . getMicroTezDelta
  toEncoding = toEncoding . StringEncode . getMicroTezDelta

instance FromJSON TezDelta where
  parseJSON x = microTezDelta . fromIntegral <$> parseJSON @TezosInt64 x

instance B.TezosBinary TezDelta where
  put t = B.put @Int64 $ fromIntegral $ getMicroTezDelta t
  get = microTezDelta <$> B.get @Int64

fromMicro :: forall a. (Integral a) => Micro -> a
fromMicro = (floor :: Fixed E6 -> a) . ((fromInteger $ resolution (Proxy :: Proxy E6)) *)

toMicro :: forall a. (Integral a) => a -> Micro
toMicro = (/ (fromInteger $ resolution (Proxy :: Proxy E6))) . (fromIntegral :: a -> Fixed E6)

instance ToMicheline TezDelta where
  toMicheline = Expression_Int . StringEncode . toInteger . getMicroTezDelta

instance FromMicheline TezDelta where
  fromMicheline = \case
    Expression_Int (StringEncode x)
      | x >= toInteger (minBound :: Int64)
      , x <= toInteger (maxBound :: Int64) -> Right $ microTezDelta $ fromInteger x
      | otherwise -> Left "mutez amount out of bounds"
    _ -> Left "unexpected encoding for mutez"