{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}

module Tezos.NodeRPC.Helpers where

import Control.Lens (each, folded, uncons)
import Control.Lens.Combinators
import Control.Lens.Operators
import Control.Monad.Except
import Control.Monad.Logger
import Control.Monad.Reader
import Data.ByteString (ByteString)
import Data.Dependent.Sum (DSum (..))
import Data.Foldable (toList)
import Data.Map (Map)
import Data.Semigroup ((<>))
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Data.Set (Set)
import Data.Word (Word64)

import Data.Aeson (FromJSON)
import qualified Data.Aeson as Aeson
import Data.Aeson.Encoding (emptyObject_)
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import qualified Data.Set as S
import Data.Text (Text)
import qualified Data.Text as T
import qualified Network.HTTP.Types.Method as Http (Method, methodGet, methodPost)

import Tezos.Common.Chain
import Tezos.Common.NetworkStat (NetworkStat)
import Tezos.Common.NodeRPC.Types
import Tezos.NodeRPC.Network (HasNodeRPC, nodeRPC)
import Tezos.V006.Contract
import Tezos.V006.Micheline
import Tezos.V006.Michelson
import Tezos.V006.NodeRPC.Class
import Tezos.V006.Operation
import qualified Tezos.V004.Account as V004
import qualified Tezos.V006.Types as V006
import qualified Tezos.V006.ProtocolConstants as V006
import qualified Tezos.V005.NodeRPC.CrossCompat as V005

dryRunEndpoint
  :: (MonadIO m
    , MonadError e m
    , AsRpcError e
    , MonadReader s m
    , HasNodeRPC s
    , MonadLogger m
    , ToMicheline a)
  => V006.ChainId
  -> V006.BlockHash
  -> V006.PublicKeyHash
  -> V006.ContractId
  -> ByteString
  -> a
  -> m OperationWithMetadata
dryRunEndpoint chain block account contract endpoint argument = do
  protocolConstants <- nodeRPC $ rProtoConstants chain block
  let gas_max = fromIntegral $ V006._protoInfo_hardGasLimitPerOperation protocolConstants
  let storage_max = fromIntegral $ V006._protoInfo_hardStorageLimitPerOperation protocolConstants
  callingAccount <- nodeRPC $ rContract (Implicit account) ChainTag_Main block
  let counter = succ $ case callingAccount of
         V005.AccountV004 acc -> V004._account_counter acc
         V005.AccountV005 acc -> fromMaybe 0 $ V006._account_counter acc
      contractParameter = V006.OpParameters (EntrypointOther $ fromMaybe (error "Invalid entrypoint name") $ entrypointName endpoint) $ toMicheline argument
      opTransfer = V006.OpContentsTransaction 0 contract $ Just contractParameter
      opContents = V006.OpContentsList_Single $ V006.OpContents_Transaction $ V006.OpContentsManager account 10 counter gas_max storage_max opTransfer
      -- This needs to fit the format of a valid signature, but is never looked at past that.
      dummySignature = Just "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"
      op = V006.OpsKindTag_Single (V006.OpKindTag_Manager V006.OpKindManagerTag_Transaction) :=> V006.Op { V006._op_branch = block, V006._op_contents = opContents, V006._op_signature = dummySignature }
  nodeRPC $ rRunOperation chain block $ OpWithChain op chain

callViewEndpoint
  :: ( MonadIO m
     , MonadError e m
     , AsRpcError e
     , MonadReader s m
     , HasNodeRPC s
     , MonadLogger m
     , ToMicheline a
     , FromMicheline b)
  => V006.ChainId
  -> V006.BlockHash
  -> V006.PublicKeyHash
  -> V006.ContractId
  -> V006.ContractId
  -> ByteString
  -> a
  -> m (Either String b)
callViewEndpoint chain block account contract tgtContract endpoint argument = do
  let finalArgument = Pair (toMicheline argument) $ toMicheline tgtContract
  result <- dryRunEndpoint chain block account contract endpoint finalArgument
  case result ^? valueFromResult of
    Just expr -> return $ fromMicheline expr
    _ -> error "Couldn't read operation"
  where
    valueFromResult
      = operationWithMetadata_contents
      . traverse
      . _OperationContents_Transaction
      . operationContentsTransaction_metadata
      . managerOperationMetadata_internalOperationResults
      . _Just
      . traverse
      . _InternalOperationResult_Transaction
      . internalOperationContentsTransaction_result
      . operationResultTransaction_storage
      . _Just
